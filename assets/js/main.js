
(function($){
	if(typeof MAIN == "undefined" ){
		MAIN = {};
	}
	
	MAIN.owlCarousel = function(){
		$('.provide_packege').owlCarousel({
		    loop:false,
		    nav:true,
		    margin:30,
		    dots:false,
		    navText: [
				'<i class="fas fa-caret-left"></i>',
				'<i class="fas fa-caret-right"></i>'
			],
		    responsive:{
		        0:{
		            items:1
		        },
		        1170:{
		            items:3
		        }
		    }
		})

		$('.top_tours').owlCarousel({
		    loop:false,
		    nav:true,
		    margin:30,
		    dots:false,
		    navText: [
				'<i class="fas fa-caret-left"></i>',
				'<i class="fas fa-caret-right"></i>'
			],
		    responsive:{
		        0:{
		            items:1
		        },
		        1170:{
		            items:3
		        }
		    }
		})
	}
	MAIN.rating = function(){
		var rating = $('.rating');
		for (var i = 0; i < 5; i++) {

			if ( i < 4 ) {
				$('<i>', {
					'class':'fas fa-star'
				}).appendTo(rating);
			}
			else {
				$('<i>', {
					'class':'fas fa-star-half-alt'
				}).appendTo(rating);
			}
		}	
	}

	$(document).ready(function(){
		MAIN.owlCarousel();
		MAIN.rating();
	});
})(jQuery);